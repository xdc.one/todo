import * as util from 'util'

class NotYetImplementedError extends Error {
  constructor(parent: NodeModule | null, name?: string) {
    const s = (name) ? name : ''
    const msg = `${s} is not yet implemented in ` + util.format(parent)
    super(msg)
  }
}

export function TODO<T>(name?: string): T {
  throw new NotYetImplementedError(module.parent, name)
}

// noinspection JSUnusedGlobalSymbols
export default function<T>(name?: string): T {
  if (imported) {
    return TODO(name)
  } else {
    imported = true
    return {} as T
  }
}

let imported: boolean = true
