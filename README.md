# XDC TODO

> ##### Latest npm package
> [![Npm version](https://img.shields.io/npm/v/@xdc/todo.svg)](https://www.npmjs.com/package/@xdc/todo)
> [![Known Vulnerabilities](https://snyk.io/test/npm/@xdc/todo/badge.svg)](https://snyk.io/test/npm/@xdc/todo)

> ##### Master branch
> [ ![Codeship Status for xdc.one/todo](https://app.codeship.com/projects/7a692790-6f21-0136-eadf-3aab8c07ba04/status?branch=master)](https://app.codeship.com/projects/298913)
> [![Codacy Badge](https://api.codacy.com/project/badge/Grade/7dba35c52568459bb394dfcbd082c998)](https://www.codacy.com/app/xdc/todo?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=xdc.one/todo&amp;utm_campaign=Badge_Grade)


## Trap ```//TODO``` functions in tests with ```TODO()``` 

> Ideal for use in boilerplate implementation code, replace //TODO comments in your new-function templates.
>
> **Installation:** ```npm i -D @xdc/todo```
-------------------------------------------

### A type safe function implementation placeholder as an alternative to TODO comments for TDD and BDD coding sessions

Provides a ```TODO``` function similar to Scala's ```???``` to call in a default function implementation when developing node modules in TypeScript. (also works in ES6)

This means you can then define new functions with explicit return types that won't cause compile errors (and IDE highlighting) but will still cause your tests to fail.

```TODO``` throws an error when called after importing, or when the module is ```require(...)```'d, and outputs the parent module's information and an optional string to specify a calling function's name. 

```
// Use via an import
import TODO from '@xdc/todo'

//
// ... some more code ...
//

// this implementation is better than //TODO...
function someNewFunction(): SomeType {
  return TODO('someNewFunction')
}

// you can omit the function name in the call
function someNewFunction(): SomeType {
  return TODO()
}

// OR use via require(...) 
// - even use a completely different function name
function someNewFunction(): SomeType {
  require('@xdc/todo').
    TODO('anotherFunctionName') // also works for import style calls
}

// OR just require the module without calling anything else
function someNewFunction(): SomeType {
  require('@xdc/todo')
} 
```

-------------------------------------------

###### License: MIT
###### Author: [XDC LTD.](https://xdc.one) (Michael Stuart-Matthews)
